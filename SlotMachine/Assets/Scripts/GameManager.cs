using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GameManager : MonoBehaviour
{
    //estas varibles acceder a la clase reels
    public Reels[] Rails;
    //almacenara el icono que esta seleccionado
    GameObject[] results;
    //Variables para hacer el timmer
    float currentTime;
    float Timer = 15;
    public Text TimerText;
    //Las monedas que se gana
    public int Coins;
    //aqui se almacenara la recompenza que se le dara
    public int prize = 0;
    public Text CoinsText;
    //este texto es el que se mostrara antes de reclamar el premio
    public Text ClaimLayerText;
    public GameObject ClaimLayer;
    bool reclaimed = false;
    //variables para los tickets
    public int Ticket;
    int MaxTickets = 7;
    float TimeTicketR = 30;
    float currentTimeTicket;
    public Text TicketText;
    bool TicketLess = false;
    public Text TimerTicketText;
    //Variables para la tienda
    public GameObject storeLayer;
    public Text TextPriceTicket;
    public Text TextPriceMulty;
    public Text TextPriceSlow;
    int PriceTicket = 700;
    int PriceMulty = 300;
    int PriceSlow = 1000;
    int CountMulty = 0;
    int CountSlow = 0;
    //poderesm de realentizar
    public GameObject SlowRailBtns;
    public Text countTextSlow;
    //multiplicador
    int multiply = 1;
    public GameObject MultiplyBtn;
    public Text countTextMulty;
    //sonidos y  musica
    public AudioSource AS;
    public AudioClip clipWin;
    public AudioClip clipLose;
    // Start is called before the first frame update
    void Start()
    {
        results = new GameObject[Rails.Length];
        currentTime = Time.time;
        currentTimeTicket = Time.time;
        MultiplyBtn.SetActive(false);
        SlowRailBtns.SetActive(false);
        if (PlayerPrefs.HasKey("Coins"))
        {
            Coins = PlayerPrefs.GetInt("Coins");
        }
        else
        {
            PlayerPrefs.GetInt("Coins", 2000);
        }
        if (PlayerPrefs.HasKey("Tickets"))
        {
            Ticket = PlayerPrefs.GetInt("Tickets");
        }
        else
        {
            PlayerPrefs.SetInt("Tickets", 7);
        }
        CountMulty = PlayerPrefs.GetInt("CounMulty");
        CountSlow = PlayerPrefs.GetInt("countSlow");

    }

    // Update is called once per frame
    void Update()
    {
        //aqui se estara actualizandola cantidad de dinero que se tiene
        CoinsText.text = Coins.ToString();
        PlayerPrefs.GetInt("Coins", Coins);
        PlayerPrefs.SetInt("CounMulty", CountMulty);
        PlayerPrefs.SetInt("countSlow", CountSlow);
        PlayerPrefs.SetInt("Tickets", Ticket);
        
        TextPriceSlow.text = PriceSlow.ToString();
        TextPriceMulty.text = PriceMulty.ToString();
        TextPriceTicket.text = PriceTicket.ToString();

        countTextMulty.text = CountMulty.ToString();
        countTextSlow.text = CountSlow.ToString();
        //llama a la funcion que se encarga de dar el premio
    }
    private void FixedUpdate()
    {
        //llama a la funcion que se encarga del timer
        TimerFuncion();
        ticketFuncion();
    }
    //aqui se hace todo lo del timer 
    void TimerFuncion()
    {
        if (AreSpinning())
        {
            if (Time.time > currentTime + 1 && Timer > 0)
            {
                Timer--;
                currentTime = Time.time;
            }
            if (Timer <= 0)
            {
                foreach (var rail in Rails)
                {
                    rail.Invoke("StopSpinning", Random.Range(1, 4));
                    if (!AreSpinning())
                    {
                        CheckHasResults();
                    }
                }
            }

        }
        TimerText.text = Timer.ToString();
    }
    //funcion que se encarga de que ver que se le dara premio y de ser asi alamacenar el premio en una variable
    void CheckHasResults()
    {
        //si preionaron detener y todos los rieles estan detenidos entonces almacenar el icono seleccionado  de no ser asi setearlo como null
        foreach (var rail in Rails)
        {
            if (rail.nearestObj != null)
            {
                results[System.Array.IndexOf(Rails, rail)] = rail.nearestObj.transform.GetChild(0).gameObject;
            }
            else
            {
                results[System.Array.IndexOf(Rails, rail)] = null;
            }
        }    
        if (AreSpinning())
        {
            reclaimed = false;
        }
        //si 1 no es null tampoco los otros en caso de que no sean null entonces que ejecute el bloque de codigo
        if (!results.Any(X=>X==null))
        {
            GivePrize();
        }
    }
    void GivePrize()
    {
        if ((results.Where(x => x.gameObject.name == results[0].gameObject.name).Count() == results.Length) && prize == 0 && reclaimed == false)
        {
            AS.clip = clipWin;
            AS.Play();
            ClaimLayer.SetActive(true);
            switch (results[0].gameObject.name)
            {
                case "1(Clone)":
                    prize = 100 * multiply;
                    break;
                case "2(Clone)":
                    prize = 100 * multiply;
                    break;
                case "3(Clone)":
                    prize = 200 * multiply;
                    break;
                case "4(Clone)":
                    prize = 200 * multiply;
                    break;
                case "5(Clone)":
                    prize = 300 * multiply;
                    break;
                case "6(Clone)":
                    prize = 300 * multiply;
                    break;
                case "7(Clone)":
                    prize = 800 * multiply;
                    break;
                case "8(Clone)":
                    prize = 400 * multiply;
                    break;
                case "9(Clone)":
                    prize = 500 * multiply;
                    break;
                case "10(Clone)":
                    prize = 500 * multiply;
                    break;
                case "11(Clone)":
                    prize = 600 * multiply;
                    break;
                case "12(Clone)":
                    prize = 600 * multiply;
                    break;
                case "BAR(Clone)":
                    prize = 1000 * multiply;
                    break;
                case "DIAMOND(Clone)":
                    prize = 2000 * multiply;
                    break;
                default:
                    break;
            }
            ClaimLayerText.text = prize.ToString();
        }
        else
        {
            if (!AS.isPlaying && Rails[0].restartBtn.activeSelf)
            {
                AS.clip = clipLose;
                AS.Play();
            }
        }
        SlowRailBtns.SetActive(false);
        MultiplyBtn.SetActive(false);
    }
    //esta funcion recarga y consume los tickets cada cierto tiempo y verfica si se tienen tickets para darle start
    void ticketFuncion()
    {
        TicketText.text = Ticket.ToString();
        TimerTicketText.text = TimeTicketR.ToString();

        if (Time.time > currentTimeTicket + 1 && TimeTicketR > 0)
        {
            TimeTicketR--;
            currentTimeTicket = Time.time;
        }

        if (TimeTicketR == 0)
        {
            TimeTicketR = 30;

            if (Ticket < MaxTickets)
            {
                Ticket++;
            }
        }

        if (Rails[0].restartBtn.activeSelf == false)
        {
            TicketLess = false;
        }

        if (Rails[0].starBtn.activeSelf == false && AreSpinning() && TicketLess == false && Ticket > 0)
        {
            Ticket = Ticket - 1;
            TicketLess = true;
        }

        foreach (var rail in Rails)
        {
            rail.canStart = Ticket != 0;
        }

    }
    //reclama el premio
    public void ClaimButton()
    {
        Coins = Coins + prize;
        prize = 0;
        reclaimed = true;
        AS.Stop();
        ClaimLayer.SetActive(false);
    }
    //cierra la tienda
    public void CloseStore()
    {
        storeLayer.SetActive(false);
        if (AreSpinning())
        {
            SlowRailBtns.SetActive(CountSlow > 0);
            MultiplyBtn.SetActive(CountMulty > 0);
        }
        
    }
    //abre la tienda
    public void openStore()
    {
        storeLayer.SetActive(true);
        SlowRailBtns.SetActive(false);
        MultiplyBtn.SetActive(false);
    }
    //compora tickets 
    public void BuyTickets()
    {
        if (Coins >= PriceTicket)
        {
            Ticket++;
            Coins = Coins - PriceTicket;
        }
    }
    //compra multiplicadores 
    public void buyMulty()
    {
        if (Coins >= PriceMulty)
        {
            CountMulty++;
            Coins = Coins - PriceMulty;
        }
    }
    //compra el realentizar 
    public void BuySlow()
    {
        if (Coins >= PriceSlow)
        {
            CountSlow++;
            Coins = Coins - PriceSlow;
        }
    }
    //relentiza un riel
    public void SlowPowerRail(int position)
    {
        if (AreSpinning())
        {
            if (CountSlow>0)
            {
                Rails[position].railSpeed = Rails[position].railSpeed / 2;
                CountSlow--;
            }
        }
    } 
    public void StopRail(int position)
    {
        Rails[position].StopSpinning();
        if (!AreSpinning())
        {
            CheckHasResults();
        }
    }
    //multiplica el premio
    public void MultiplyFunction()
    {
        if (AreSpinning())
        {
            multiply = multiply * 2;
            CountMulty--;
        }
    }
    public void RestartAll()
    {
        if (AreSpinning())
        {
            return;
        }
        foreach (var rail in Rails)
        {
            rail.RestartInteraction();
        }
        Timer = 15;
        foreach (var rail in Rails)
        {
            rail.railSpeed = Reels.normalSpeed;
        }
        multiply = 1;
        if (AS.isPlaying)
        {
            AS.Stop();
        }
    }
    public void StartAll()
    {
        foreach (var rail in Rails)
        {
            rail.StarInteraction();
        }
        SlowRailBtns.SetActive(CountSlow>0);
        MultiplyBtn.SetActive(CountMulty>0);
    }
    bool AreSpinning()
    {
        foreach (var rail in Rails)
        {
            if (rail.isSpinning)
            {
                return true;
            }
        }
        return false;
    }
}
