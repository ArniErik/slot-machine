using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reels : MonoBehaviour
{
    //el objeto mas cercano 
    public GameObject nearestObj = null;
    //alamcena los objetos que contendran los iconos
    public GameObject[] arrContainer;
    //alamcena los iconos
    public GameObject[] arrOptions;
    //es el objeto que dira quien icono fue el seleccionado
    public GameObject trigger;
    //velocidad del los rieles
    public float railSpeed = 0.1f;
    [SerializeField]
    public static float normalSpeed= 0.3f;
    //el punto en el que despawnean para volver a aparecer
    float dSpawnPoint;
    //mira si esta girando o no
    public bool isSpinning = false;
    // permite girar el riel
    public bool canStart = true;
    //el boton de start
    public GameObject starBtn;
    //el boton de restart
    public GameObject restartBtn;
    //sonido de girar
    AudioSource AS;
    float nearest;
    // Start is called before the first frame update
    void Start()
    {
        //se ponen los iconos de manera aleatoria en el riel
        SetIcons();
        //el momento en el que los iconos desaparesen 
        dSpawnPoint = arrContainer[0].transform.position.y - 3.6f;
        AS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
     
        //si sppinign esta en verdadero hara girar el riel de lo contrario lo va a parar
        if (isSpinning)
        {
            StartSpinnign();
        }
        else if (nearestObj != null)
        {
            Vector3 distanceA= new Vector3(0,0,0);
            distanceA = nearestObj.transform.position - trigger.transform.position;
            if (distanceA.y < nearest)
            {
                if (distanceA.y > 0)
                {
                    nearest = distanceA.y;
                }
            }
            if (nearestObj.transform.position.y != trigger.transform.position.y)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - (nearest / 10));

            }
        }

    }

    // destruye todo los iconos y deja vacio al riel
    void DestroyItem()
    {
        for (int i = 0; i < arrContainer.Length; i++)
        {
            if (arrContainer[i].transform.GetChild(0).gameObject != null)
            {
                Destroy(arrContainer[i].transform.GetChild(0).gameObject);
            }
        }
    }

    // spawnea de manera aleatoria iconos en todo el riel 
    void SetIcons()
    {
        for (int i = 0; i < arrContainer.Length; i++)
        {
            Instantiate(arrOptions[Random.Range(0, arrOptions.Length-1)], arrContainer[i].transform);
        }

    }

    //hace girar al riel 
    void StartSpinnign()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - railSpeed);
        if (AS.isPlaying==false)
        {
            AS.Play();
        }
        Vector3 newPosition;
        for (int i = 0; i < arrContainer.Length; i++)
        {
            if (arrContainer[i].transform.position.y <= dSpawnPoint)
            {
                newPosition = arrContainer[arrContainer.Length - 1].transform.position;
                newPosition.y = newPosition.y + 1.8f * (i + 1);
                arrContainer[i].transform.position = newPosition;
            }
        }
    }

    //detiene el riel y para en el icono mas sercano hacia arriba
    public void StopSpinning()
    {
        isSpinning = false;
        Vector3 distance = new Vector3(0, 0, 0);
        nearest = 10;
        nearestObj = null;
        for (int i = 0; i < arrContainer.Length; i++)
        {
            distance = arrContainer[i].transform.position - trigger.transform.position;
            if (distance.y < nearest)
            {
                if (distance.y > 0)
                {
                    nearest = distance.y;
                    nearestObj = arrContainer[i];
                }
            }
        }

        if (AS.isPlaying)
        {
            AS.Stop();
        }
        
    }
    //esta funcion sera llamada por el boton que hara girar todos los rieles 
    public void StarInteraction()
    {
        if (canStart)
        {
            if (isSpinning==false)
            {
                isSpinning = true;
                starBtn.SetActive(false);
                restartBtn.SetActive(true);
            }
        }
    }
    public void RestartInteraction()
    {
        if (isSpinning == false)
        {
            DestroyItem();
            SetIcons();
            restartBtn.SetActive(false);
            starBtn.SetActive(true);
        }
    }


}
